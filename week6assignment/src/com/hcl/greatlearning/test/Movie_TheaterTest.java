package com.hcl.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;


import com.hcl.greatlearning.bean.Movie_Theater;


public class Movie_TheaterTest {

	public void testMovies_In_Theaters() {
		fail("Not yet implemented");
	}

//	@Test
	public void testMovies_In_TheatersIntStringIntStringStringInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		System.out.println("getId");
		Movie_Theater mit=new Movie_Theater();
		int expResult=1;
		mit.setId(1);
		int result=mit.getId();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetId() {
		//fail("Not yet implemented");
		System.out.println("setId");
		int id=1;
		Movie_Theater mit=new Movie_Theater();
		mit.setId(id);
		assertEquals(mit.getId(),id);
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		System.out.println("getTitle");
		Movie_Theater mit=new Movie_Theater();
		String expResult="Black Panther";
		mit.setTitle("Black Panther");
		String result=mit.getTitle();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		System.out.println("setTitle");
		String title="Black Panther";
		Movie_Theater mit=new Movie_Theater();
		mit.setTitle(title);
		assertEquals(mit.getTitle(),title);
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		System.out.println("getYear");
		Movie_Theater mit=new Movie_Theater();
		int expResult=2018;
		mit.setYear(2018);
		int result=mit.getYear();
		assertEquals(expResult,result);

	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		System.out.println("setYear");
		int year=2018;
		Movie_Theater mit=new Movie_Theater();
		mit.setYear(year);
		assertEquals(mit.getYear(),year);
	}

	@Test
	public void testGetGenres() {
		//fail("Not yet implemented");
		System.out.println("getGenres");
		Movie_Theater mit=new Movie_Theater();
		String expResult="Action";
		mit.setGenres("Action");
		String result=mit.getGenres();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetGenres() {
		//fail("Not yet implemented");
		System.out.println("setGenres");
		String genres="Action";
		Movie_Theater mit=new Movie_Theater();
		mit.setGenres(genres);
		assertEquals(mit.getGenres(),genres);
	}

	@Test
	public void testGetDuration() {
		//fail("Not yet implemented");
		System.out.println("getDuration");
		Movie_Theater mit=new Movie_Theater();
		String expResult="PT134M";
		mit.setDuration("PT134M");
		String result=mit.getDuration();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetDuration() {
		//fail("Not yet implemented");
		System.out.println("setDuration");
		String duration="PT134M";
		Movie_Theater mit=new Movie_Theater();
		mit.setDuration(duration);
		assertEquals(mit.getDuration(),duration);
	}

	@Test
	public void testGetRelease_date() {
		//fail("Not yet implemented");
		System.out.println("getRelease_date");
		Movie_Theater mit=new Movie_Theater();
		int expResult=2018-02-14;
		mit.setRelease_date(2018-02-14);
		int result=mit.getRelease_date();
		assertEquals(expResult,result);

	}

	@Test
	public void testSetRelease_date() {
		//fail("Not yet implemented");
		System.out.println("setRelease_date");
		int release_date=2018-02-14;
		Movie_Theater mit=new Movie_Theater();
		mit.setRelease_date(release_date);
		assertEquals(mit.getRelease_date(),release_date);
	}

}
