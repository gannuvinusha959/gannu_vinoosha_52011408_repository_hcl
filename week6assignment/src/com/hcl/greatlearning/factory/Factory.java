package com.hcl.greatlearning.factory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hcl.greatlearning.bean.Coming_Up_Movies;
import com.hcl.greatlearning.bean.Movie_Theater;
import com.hcl.greatlearning.bean.Indian_Movies;
import com.hcl.greatlearning.bean.Top_Rated_Movies;
import com.hcl.greatlearning.resource.ResourceData;


interface MovieOnline  {
	
	public List<String> movieType() throws Exception;

}

class MovieComing implements MovieOnline{
	
	Connection con=ResourceData.getDbConnection();
	public MovieComing () {}
	@Override
	public List<String> movieType() throws Exception {
		
		try {
			List<Coming_Up_Movies> movies=new ArrayList<Coming_Up_Movies>();
			PreparedStatement pstmt=con.prepareStatement("select * from Coming_Up_Movies");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Coming_Up_Movies movie=new Coming_Up_Movies();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setRelease_date(rs.getInt(6));
				movies.add(movie);
			}}catch(Exception e) {
				System.out.println("Coming_Up_Movies"+e);
			}
		return null;
	}
	
}
class MovieInTheatres implements MovieOnline{
	//Database Connection
	Connection con=ResourceData.getDbConnection();
	public MovieInTheatres() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Movie_Theater> movies=new ArrayList<Movie_Theater>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movie_Theater");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Movie_Theater movie=new Movie_Theater();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setRelease_date(rs.getInt(6));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Movie_Theater"+e);
		}
	    return null;	
	}
	
}
class TopRatedIndianMovie implements MovieOnline{
	Connection con=ResourceData.getDbConnection();
	public TopRatedIndianMovie() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Indian_Movies> movies=new ArrayList<Indian_Movies>();
			PreparedStatement pstmt=con.prepareStatement("select * from Indian_Movies");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Indian_Movies movie=new Indian_Movies();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenres(rs.getString(3));
				movie.setDuration(rs.getString(4));
				movie.setRelease_date(rs.getInt(5));
				movie.setImdbRating(rs.getInt(6));
				
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Indian_Movie"+e);
		}
	    return null;	
	}
	
	
}
class TopRatedMovie implements MovieOnline{
	Connection con=ResourceData.getDbConnection();
	public TopRatedMovie() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Top_Rated_Movies> movies=new ArrayList<Top_Rated_Movies>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Movies");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Top_Rated_Movies movie=new Top_Rated_Movies();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenres(rs.getString(3));
				movie.setDuration(rs.getString(4));
				movie.setRelease_date(rs.getInt(5));
				movie.setImdbRating(rs.getInt(6));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Movies"+e);
		}
	    return null;	
	}
}
	
class MovieFactory{
	public static MovieOnline getInstance(String type) {
		if(type.equalsIgnoreCase("MovieComing")) {
			return new MovieComing();
		}else if(type.equalsIgnoreCase("MovieInTheatres")) {
			return new MovieInTheatres();
		}else if(type.equalsIgnoreCase("TopRatedIndianMovie")) {
			return new TopRatedIndianMovie();
		}
		else if(type.equalsIgnoreCase("TopRatedMovie")) {
			return new TopRatedMovie();
		}else {
	    	return null;
	}
}
}

public class Factory {
	public static void main(String[] args) throws Exception {
		
	MovieOnline mo =MovieFactory.getInstance("Coming_Up_Movies");
	mo.movieType();
    MovieOnline mo1 =MovieFactory.getInstance("MovieTheater");
    mo1.movieType();
    MovieOnline mo2 =MovieFactory.getInstance("IndianMovie");
    mo2.movieType();
    MovieOnline mo3=MovieFactory.getInstance("TopRatedMovies");
    mo3.movieType();
    
    
	}
}
