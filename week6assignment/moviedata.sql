create database movie_vinoosha_hcl;
use movie_vinoosha_hcl;
 

create table Coming_Up_Movies(id int primary key,title varchar(100),year int,genres varchar(100),duration varchar(100),release_date date);

insert into Coming_Up_Movies values(1,'Game Night',2018,'Action','PT100M','2018-02-28');
insert into Coming_Up_Movies values(2,'Area X Annihilation',2018,'Adventure','PT100M','2018-02-23');
insert into Coming_Up_Movies  values(3,'Hannah',2017,'Drama','PT95M','2018-01-24');
insert into Coming_Up_Movies values(4,'The Lodgers',2017,'Drama','PT92M','2018-03-09');
 insert into Coming_Up_Movies values(5,'Beast of Burden',2018,'Action','PT94M','2018-02-01');

select * from Coming_Up_Movies;

 create table Movie_Theater(id int primary key,title varchar(100),year int,genres varchar(100),duration varchar(100),release_date date);

 insert into Movie_Theater values(1,'Black Panther',2018,'Action','PT134M','2018-02-14');
 insert into Movie_Theater values(2,'GrottmannenDug',2018,'Animation','PT89M','2018-03-23');
 insert into Movie_Theater values(3,'Aiyaary',2018,'Action','PT157M','2018-02-16');
 insert into Movie_Theater values(4,'Samson',2018,'Action','PT156M','2018-02-16');
 insert into Movie_Theater values(5,'Samson',2017,'Drama','PT12TM','2017-06-01');

select * from Movie_Theater;

 create table Indian_Movie(title varchar(100) primary key,year int,genres varchar(100),duration varchar(100),released_date date,imdbRating int);

 insert into Indian_Movies values('Anand',1971,'Drama','PT122M','1971-03-12',8);
insert into Indian_Movies values('Dangal',2016,'Biography','PT161M','2003-01-14',8.6);
 insert into Indian_Movies values('Anbe sivam',2003,'Adventure','PT160M','2003-12-23',8.9);
 insert into Indian_Movies values('Gol Maal',1979,'Comedy','PT144M','1979-04-20',8.7);
 insert into Indian_Movies values('Black Friday',2007,'Crime','PT143M','2007-02-09',8.6);

select * from Indian_Movies;

 create table Top_Rated_Movies(title varchar(100) primary key,year int,genres varchar(100),duration varchar(100),released_date date,imdbRating int);

insert into Top_Rated_Movies values('Det Sjunde Inseglet',1957,'Fantasy','PT96M','1957-02-16',8);
 insert into Top_Rated_Movies values('Dom Nurnberg',1961,'War','PT186M','1961-12-18',8.3);
 insert into Top_Rated_Movies values('Nawals Hemligher',2010,'Mystrey','PT131M','2011-08-12',8.2);
insert into Top_Rated_Movies values('A Beautiful Mind',2001,'Biography','PT135M','2002-03-08',8.2);
insert into Top_Rated_Movies values('Eskiya',1996,'Crime','PT128M','1996-11-29',8.5);

select * from Top_Rated_Movies; 
